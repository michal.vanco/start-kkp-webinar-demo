# start.kubermatic - Manual Deployment Steps for GitLab

Here's the list of exact steps to be followed if you want to validate everything yourself
(without having that delivered with GitLab CI/CD pipeline).

We will make use of GitLab Terraform State.

This can be handy for testing everything locally, understanding the whole flow and to be able to customize
for your needs later on.

## Preparation

### Required Tools

 * `git`
 * `terraform` (1.0.4+)
 * `kubeone` (1.3.0+)
 * `sops` (3.7.1+)
 * `flux` v2 (0.16.1+)

### Setup env.variables

```bash
# your Google Service Account key, see README.md (Get your gce credentials)
export GOOGLE_CREDENTIALS=$(cat ./k1-cluster-provisioner-sa-key.json)
# from GitLab settings
export GITLAB_TOKEN=<GITLAB_TOKEN>
# your GitLab owner / username / organization
export GITLAB_OWNER=<GITLAB_OWNER>
# your GitLab repository name
export GITLAB_REPOSITORY=<GITLAB_REPOSITORY>
```

### Prepare GitLab repository

Create a project on your GitLab instance with `<GITLAB_OWNER>` and `<GITLAB_REPOSITORY>`.

```bash
git init
git checkout -b main
# do not add a .gitlab-ci.yml file!
git add README.md .gitignore flux/ kubermatic/ terraform/ kubeone/
git commit -m "Initial setup for KKP on Autopilot"
git remote add origin git@gitlab.com:$GITLAB_OWNER/$GITLAB_REPOSITORY.git
git push -u origin main
```

## Deployment

### Prepare SSH key-pair

```bash
ssh-keygen -t rsa -b 4096 -C "<ADMIN_EMAIL>" -f ~/.ssh/k8s_rsa
```

If you choose a different location for the key, make sure to update the `ssh_public_key_file` variable in `terraform.tfvars`!

### Prepare infrastructure with Terraform

```bash
cd terraform/gce
export GITLAB_PROJECT_ID=<GITLAB_PROJECT_ID> # get project ID from GitLab UI or API
export GITLAB_TF_STATE_NAME=kkp              # name of the Gitlab Terraform state
export GITLAB_USERNAME=<GITLAB_USERNAME>     # your GitLab user associated with GITLAB_TOKEN
terraform init \
  -backend-config="address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${GITLAB_TF_STATE_NAME}" \
  -backend-config="lock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${GITLAB_TF_STATE_NAME}/lock" \
  -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${GITLAB_TF_STATE_NAME}/lock" \
  -backend-config="username=${GITLAB_USERNAME}" \
  -backend-config="password=${GITLAB_TOKEN}" \
  -backend-config="lock_method=POST" \
  -backend-config="unlock_method=DELETE" \
  -backend-config="retry_wait_min=5"
eval `ssh-agent`
ssh-add ~/.ssh/k8s_rsa
# due to how GCP LBs work, initial terraform apply requires variable control_plane_target_pool_members_count to be set to 1
terraform apply -var=control_plane_target_pool_members_count=1
terraform output -json > output.json
cd ../..
```

### Initialize k8s cluster with KubeOne

```bash
cd kubeone
kubeone apply -m kubeone.yaml -t ../terraform/gce/output.json -y -v
export KUBECONFIG=`pwd`/undefined-boyd-kubeconfig
cd ..
```
### Additional Terraform run

Now you can run again `terraform apply` to make your LB HA (this step is only happening on GCP after first run).

```bash
cd terraform/gce
terraform apply
cd ../..
```

### Deploy KKP

We need to decrypt the kkp-config and helm values, put the secret key from secrets.md in `.age.key`.

```bash
export SOPS_AGE_KEY_FILE=`pwd`/.age.txt
cd kubermatic
sops -d -i kubermatic-configuration.yaml
sops -d -i values.yaml
export VERSION=v2.19.0
wget https://github.com/kubermatic/kubermatic/releases/download/${VERSION}/kubermatic-ce-${VERSION}-linux-amd64.tar.gz
tar -xzvf kubermatic-ce-${VERSION}-linux-amd64.tar.gz
./kubermatic-installer deploy --config kubermatic-configuration.yaml --helm-values values.yaml --storageclass gce
cd ..
```

### Prepare other resources for KKP and Flux

```bash
# Cluster Issuer for cert-manager (let's encrypt)
kubectl apply -f kubermatic/cluster-issuer.yaml
# kubeconfig Secret to be used for Seed cluster configuration
kubectl create secret generic kubeconfig-cluster -n kubermatic --from-file=kubeconfig=`pwd`/kubeone/undefined-boyd-kubeconfig --dry-run=client -o yaml | kubectl apply -f -
# Values for helm releases are able to read secret only from same namespace so we need values in all NS
kubectl create secret generic kubermatic-values -n logging --from-file=values.yaml=`pwd`/kubermatic/values.yaml --dry-run=client -o yaml | kubectl apply -f -
kubectl create secret generic kubermatic-values -n monitoring --from-file=values.yaml=`pwd`/kubermatic/values.yaml --dry-run=client -o yaml | kubectl apply -f -
kubectl create secret generic kubermatic-values -n iap --from-file=values.yaml=`pwd`/kubermatic/values.yaml --dry-run=client -o yaml | kubectl apply -f -
cat .age.txt | kubectl -n kubermatic create secret generic sops-age --from-file=age.agekey=/dev/stdin --dry-run=client -o yaml | kubectl apply -f -
```

### Update DNS records

At this point, you should manage the DNS records the Kubermatic Kubernetes platform.

See the related [documentation](https://docs.kubermatic.com/kubermatic/master/guides/installation/install_kkp_ce/#create-dns-records).

### Initialize Flux v2

```bash
flux bootstrap gitlab --owner=$GITLAB_OWNER --repository=$GITLAB_REPOSITORY \
  --branch=main --personal=true --path flux/clusters/master
```

Your Kubermatic Kubernetes Platform is ready to use now, look around
all deployed services and custom resources in Kubernetes.

More cheat sheets to follow are summarized in [documentation](https://docs.kubermatic.com/kubermatic/master/installation/start_kkp/cheat_sheets).
