image:
  name: quay.io/kubermatic/startio-ci:v0.2.4-0
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

stages:
  - tf-validate
  - tf-plan
  - tf-apply
  - kubeone-apply
  - kkp-deploy
  - flux-bootstrap

variables:
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}
  TF_ROOT: ${CI_PROJECT_DIR}/terraform/gce
  KUBEONE_ROOT: ${CI_PROJECT_DIR}/kubeone
  KUBERMATIC_ROOT: ${CI_PROJECT_DIR}/kubermatic
  KUBERMATIC_VERSION: v2.19.0

.kkp:
  environment:
    name: kkp
  variables:
    ENV_NAME: kkp
    TF_STATE_NAME: kkp

.ssh-before:
  before_script:
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh && cp $SSH_PUBLIC_KEY ~/.ssh/k8s_rsa.pub

# Run checks on terraform module for k8s
terraform-kkp-validate:
  stage: tf-validate
  script:
    - cd $TF_ROOT
    - gitlab-terraform init -backend=false
    - gitlab-terraform validate
    - gitlab-terraform fmt -check -recursive -write=false
  only:
    changes:
      - .gitlab-ci.yml
      - terraform/gce/*

# Run terraform plan (using the state from GitLab)
tf-plan:
  stage: tf-plan
  extends:
    - .kkp
    - .ssh-before
  script:
    - cd ${TF_ROOT}
    - |
      # due to how GCP LBs work, initial terraform apply requires variable control_plane_target_pool_members_count to be set to 1
      # check if there are is output defined, if not - it should be the first run
      gitlab-terraform init
      # need to check the exit code by setting a variable to avoid cancelling the job
      gitlab-terraform output -json kubeone_api || export FIRST_RUN=true
      if [ $FIRST_RUN == true ]; then
        echo "Running terraform with extra parameter for LB"
        gitlab-terraform plan -var=control_plane_target_pool_members_count=1
        touch first-run
      else
        gitlab-terraform plan
      fi
    - gitlab-terraform plan-json
  artifacts:
    name: ${ENV_NAME}-plan
    paths:
      - ${TF_ROOT}/plan.cache
      - ${TF_ROOT}/first-run
    reports:
      terraform: ${TF_ROOT}/plan.json
  only:
    changes:
      - .gitlab-ci.yml
      - terraform/gce/*

# Run terraform apply to setup k8s infrastructure
# Only executed on "push" event to `main` branch
tf-apply:
  stage: tf-apply
  extends:
    - .kkp
    - .ssh-before
  dependencies:
    - tf-plan
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform apply
  only:
    changes:
      - .gitlab-ci.yml
      - terraform/gce/*
    refs:
      - main

# Provision / update the k8s cluster with KubeOne
# Only executed on "push" event to `main` branch
kubeone-apply:
  stage: kubeone-apply
  extends:
    - .kkp
    - .ssh-before
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform init
    # retrieve TF output from TF state
    - gitlab-terraform output -json > output.json
    - cd ${KUBEONE_ROOT}
    # provision and manage master cluster
    - kubeone apply -m kubeone.yaml -t ../terraform/gce/output.json -y -v
    # perform additional terraform apply if needed (due to LB to become HA), only for first run!
    - |
      if [[ -f ${TF_ROOT}/first-run ]]; then
        cd ${TF_ROOT}
        # remove the plan file from initial run to apply the fresh state
        rm -f ${TF_ROOT}/plan.cache
        gitlab-terraform plan
        gitlab-terraform apply
      fi
  dependencies:
    - tf-plan
  only:
    changes:
      - .gitlab-ci.yml
      - kubeone/**/*
    refs:
      - main

# Install KKP on k8s cluster with operator (installer)
# Only executed on "push" event to `main` branch
kkp-deploy:
  stage: kkp-deploy
  extends:
    - .kkp
    - .ssh-before
  script:
    # retrieve TF output from TF state
    - cd ${TF_ROOT}
    - gitlab-terraform init
    - gitlab-terraform output -json > output.json
    # retrieve kubeconfig of k8s cluster
    - cd ${KUBEONE_ROOT}
    - kubeone kubeconfig -m kubeone.yaml -t ../terraform/gce/output.json > kubeconfig
    - export KUBECONFIG=${KUBEONE_ROOT}/kubeconfig
    - cd ${KUBERMATIC_ROOT}
    # decrypt config files
    - echo ${SOPS_AGE_SECRET_KEY} > .age.txt
    - export SOPS_AGE_KEY_FILE=${CI_PROJECT_DIR}/kubermatic/.age.txt
    - sops -d -i kubermatic-configuration.yaml
    - sops -d -i values.yaml
    # run KKP installer
    - wget https://github.com/kubermatic/kubermatic/releases/download/${KUBERMATIC_VERSION}/kubermatic-ce-${KUBERMATIC_VERSION}-linux-amd64.tar.gz
    - tar -xzvf kubermatic-ce-${KUBERMATIC_VERSION}-linux-amd64.tar.gz
    - ./kubermatic-installer deploy --config kubermatic-configuration.yaml --helm-values values.yaml --storageclass gce
    # initialize other KKP resources
    - |
      # Cluster Issuer for cert-manager (let's encrypt)
      kubectl apply -f ${KUBERMATIC_ROOT}/cluster-issuer.yaml
      # kubeconfig Secret to be used for Seed cluster configuration
      kubectl create secret generic kubeconfig-cluster -n kubermatic \
        --from-file=kubeconfig=${KUBEONE_ROOT}/kubeconfig --dry-run=client -o yaml | kubectl apply -f -
      kubectl create secret generic kubermatic-values -n kubermatic \
        --from-file=values.yaml=${KUBERMATIC_ROOT}/values.yaml --dry-run=client -o yaml | kubectl apply -f -
      # Values for helm releases are able to read secret only from same namespace so we need values in all NS
      
      kubectl create secret generic kubermatic-values -n logging \
        --from-file=values.yaml=${KUBERMATIC_ROOT}/values.yaml --dry-run=client -o yaml | kubectl apply -f -
      kubectl create secret generic kubermatic-values -n monitoring \
        --from-file=values.yaml=${KUBERMATIC_ROOT}/values.yaml --dry-run=client -o yaml | kubectl apply -f -
      kubectl create secret generic kubermatic-values -n iap \
        --from-file=values.yaml=${KUBERMATIC_ROOT}/values.yaml --dry-run=client -o yaml | kubectl apply -f -
      
      cat ${KUBERMATIC_ROOT}/.age.txt | kubectl -n kubermatic create secret generic sops-age \
        --from-file=age.agekey=/dev/stdin --dry-run=client -o yaml | kubectl apply -f -
  dependencies: []
  only:
    changes:
      - .gitlab-ci.yml
      - kubermatic/**/*
    refs:
      - main

# Initialize GitOps tooling (Flux v2) on the current repository
# Only executed on "push" event to `main` branch
flux-bootstrap:
  stage: flux-bootstrap
  extends:
    - .kkp
    - .ssh-before
  script:
    - |
      if [ -d flux/clusters/master/flux-system ]; then
        echo "Flux was already bootstrapped on this repository, no action needed."
      else
        echo "Bootstrapping Flux on k8s cluster."
        cd ${TF_ROOT}
        gitlab-terraform init
        gitlab-terraform output -json > output.json
        cd ${KUBEONE_ROOT}
        kubeone kubeconfig -m kubeone.yaml -t ../terraform/gce/output.json > kubeconfig
        export KUBECONFIG=${KUBEONE_ROOT}/kubeconfig
        flux bootstrap gitlab \
          --owner=${CI_PROJECT_NAMESPACE} \
          --repository=${CI_PROJECT_NAME} \
          --branch=main \
          --personal=true \
          --path=flux/clusters/master \
          --commit-message-appendix='[ci skip]'
      fi
  dependencies: []
  only:
    refs:
      - main
